- Tạo database mongo với tên graphTracking, thêm 3 các collection tương ứng với các file json trong thư mục database.
- Download các package cần thiết với lệnh ```npm install``` trong cmd.
- Khởi động database.
- Chạy server với lệnh npm start.
- Dùng hàm drawShipRoute(startPort, endPort) trong file testindex.html để vẽ đường đi giữa 2 cảng, với startPort là tên cảng bắt đầu, endPort là tên cảng kết thúc.