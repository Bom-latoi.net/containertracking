const vertexModel = require('../models/vertex');

const getAllEdgesCoordinate = (edgesListByID, edgesListByCoordinate) => {
  return new Promise((resolve, reject) => {
    vertexModel.find({})
      .sort({_id: 1})
      .exec((err, vertexes) => {
        if(err) reject(err);
        
        edgesListByID.forEach(edges => {
          if(vertexes[edges[0]-1] && vertexes[edges[1]-1])
          edgesListByCoordinate.push([
            vertexes[edges[0]-1].coordinate,
            vertexes[edges[1]-1].coordinate
          ])
        })
        resolve(edgesListByCoordinate);
      })
  })
}

module.exports = getAllEdgesCoordinate;