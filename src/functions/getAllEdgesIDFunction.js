const adjacentList = require('../models/adjacentList');

const getAllEdgesID = edgesListByID => {
  return new Promise((resolve, reject) => {
    adjacentList.find({})
      .sort({_id: 1})
      .exec((err, docs) => {
        if(err) reject(err);
        
        docs.forEach(srcNode => {
          srcNode.adjacentVertex.forEach(desNode => {
            if(desNode._id > srcNode._id){
              edgesListByID.push([srcNode._id ,desNode._id]);
            }
          })
        })
        
        resolve(edgesListByID)
      })
  })
}

module.exports = getAllEdgesID;