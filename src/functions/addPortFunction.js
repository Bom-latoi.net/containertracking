const portModel = require('../models/port');

const addPort = (port) => {
  portModel.find({})
    .sort({'_id': -1})
    .exec((err, docs) => {
    if (err) throw err;
  
    let id = docs[0]? docs[0]._id + 1 : 1;

    let portInfo = new portModel({
      _id: id,
      name: port.name,
      codeID: port.codeID,
      coordinateID: port.coordinateID
    })

    portInfo.save()
      .then(doc => {
        console.log(doc._id)
      })
      .catch(console.error)
  })
}

module.exports = addPort;