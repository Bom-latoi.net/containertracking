const vertexModel = require('../models/vertex');
const addEdgeFunction = require('../functions/addEdgeFunction');

module.exports = (listID) => {
  vertexModel
    .find({"_id": {$in: listID}})
    .select('_id coordinate')
    .sort('_id')
    .exec((err, docs) => {
      if(err) throw err;
      for(let i = 0; i < docs.length - 1; i++){
        addEdgeFunction(docs[i], docs[i+1]);
      }
    }) 
}


