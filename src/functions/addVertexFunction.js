const vertexModel = require('../models/vertex');
const addEdgeByIDFunction = require('../functions/addEdgeByIDFunction');

const addVertex = (vertexCoordinateList) => {
  vertexModel.find({})
    .sort({'_id': -1})
    .exec(async(err, docs) => {
    if (err) throw err;
  
    let startId = docs[0]? docs[0]._id + 1 : 1;
    let type = docs[0]? docs[0].type + 1: 1;
    let listNewID = [];
    
    let id = startId;
    await Promise.all(
        vertexCoordinateList.map(vertexCoor => {
        docID = id;
        id++;
        return saveVertex(docID, type, vertexCoor, listNewID);
      })
    )
    
    addEdgeByIDFunction(listNewID);
  })
}

const saveVertex = (id, type, vertexCoor, listNewID = []) => {
  return new Promise((resolve, reject) => {
    let vertex = new vertexModel({
      _id: id,
      coordinate: vertexCoor,
      area: [Math.ceil((90-vertexCoor[0])/5), Math.ceil((180-vertexCoor[1])/5)],
      type: type
    })
    
    vertex.save()
      .then(doc => {
        console.log(doc._id);
        listNewID.push(doc._id);
        resolve();
      })
      .catch(reject)
  })  
}

module.exports = addVertex;