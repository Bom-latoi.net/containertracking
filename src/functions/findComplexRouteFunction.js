const vertexModel = require('../models/vertex');
const {getArea, euclidDistance} = require('../utils/utils');
const {buildGraph} = require('./buildGraphFunction');
const {findPortID} = require('./findPortIDFunction');

const debug = false;

const findComplexRoute = async (response, listPortName, lastPortVisitedName, shipCoordinate) => {
  //======================DEBUG LOG=======================
  debug && console.log('\n====FUNCTION: findComplexRoute====');
  debug && console.log('>>-------------Input----------------');
  debug && console.log('- Port list:' + listPortName);
  debug && console.log('- Last visited port: ' + lastPortVisitedName);
  debug && console.log('- ShipCoordinate: ' + shipCoordinate);
  //======================================================
  
  let graph = await buildGraph();
  let finalRoute = [];

  let listPortID = await Promise.all(listPortName.map(portName => findPortID(portName)));
  let lastPortVisitedID = await findPortID(lastPortVisitedName);
  let shipArea = getArea(shipCoordinate);
  let nearestPoint = await findNearestPoints(shipArea, shipCoordinate);
  let break1 = listPortID.indexOf(lastPortVisitedID);
  if((nearestPoint != -1) && (break1 === listPortID.length-1)){
    //////////////////////////////////////////////////////////////
    //                                                          //
    //                   lastPort            ship               //
    // >>>>>>>>>>>>>>>>>>>>>||>>>>>>>>>>>>>>>>||>>>>>>>         //
    //            (1)               (2)           route         //
    //                                                          //
    //////////////////////////////////////////////////////////////

    //route (1)
    for(let i = 0; i < listPortID.length-1; i++){
      finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[i], listPortID[i+1]));
      finalRoute.pop();
    }

    //route(2)
    finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[listPortID.length-1], nearestPoint));
    
    //replace nearestPoint coordinate by shipCoordinate
    finalRoute[finalRoute.length-1] = shipCoordinate.slice(',').map(Number);
  }
  else if(nearestPoint != -1){
    
    //////////////////////////////////////////////////////////////
    //                                                          //
    //        break1    nearestPoint   break1+1                    //
    // >>>>>>>>>||>>>>>>>>>||>>>>>>>>>>||>>>>>>>>>>>>>>>>>>>    //
    //    (1)       (2)         (3)           (4)       route   //
    //                                                          //
    //////////////////////////////////////////////////////////////
    
    //======================DEBUG LOG=======================
    debug && console.log('>>-------------Convert to ID---------------');
    debug && console.log('- Port list ID:' + listPortID);
    debug && console.log('- Last visited port ID: ' + listPortID[break1]);
    debug && console.log('- Temp point ID: ' + nearestPoint);
    //======================================================
    
    //route (1)
    for(let i = 0; i<break1; i++){
      finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[i], listPortID[i+1]));
      finalRoute.pop();
    }
    
    //route (2)
    finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[break1], nearestPoint));
    finalRoute.pop();
    
    let nearestPointIndex = finalRoute.length;
    
    //route (3)
    finalRoute.push(...findRouteBetweenTwoID(graph, nearestPoint, listPortID[break1+1]));
    finalRoute.pop();
    
    //replace nearestPoint coordinate by shipCoordinate
    finalRoute.splice(nearestPointIndex, 1, shipCoordinate.slice(',').map(Number));

    //route (4)
    for(let i = break1 + 1; i < listPortID.length-1 ; i++){
      finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[i], listPortID[i+1]));
    }
  }
  else{
    for(let i = 0; i < listPortID.length-1; i++){
      finalRoute.push(...findRouteBetweenTwoID(graph, listPortID[i], listPortID[i+1]));
    }
  }
  
  response.json(finalRoute);
}

const findRouteBetweenTwoID = (graph, ID1, ID2) => {
  //==============DEBUG LOG=================
  debug && console.log('\n====FUNCTION: findRouteBetweenTwoID===');
  debug && console.log('- Route: ' + ID1 + '--->' + ID2);
  //========================================

  return graph.UCS(ID1, ID2);
}

const findNearestPoints = (shipArea, shipCoordinate) => {
  //===================DEBUG LOG=================
  debug && console.log('\n====FUNCTION: findNearestPoints===');
  debug && console.log("- All points in area: " + shipArea);
  //=============================================

  return new Promise((resolve, reject) => {
    vertexModel.find({area: shipArea})
      .select('_id coordinate')
      .exec((err, points) => {
        if (err) throw reject(err);
        if (points.length === 0){
          resolve(-1);
          return;
        } 
        
        //===================DEBUG LOG=================
        debug && console.log('\n====FUNCTION: findNearestPoints===');
        debug && console.log("- All points in area: " + points);
        //=============================================

        let minDistance = euclidDistance(shipCoordinate, points[0].coordinate);
        let nearestPointId = points[0]._id;
        for(let i = 1; i < points.length; i++){
          let distance = euclidDistance(shipCoordinate, points[i].coordinate);
          if(distance < minDistance){
            minDistance = distance;
            nearestPointId = points[i]._id;
          }
        }

        resolve(nearestPointId)
      })
  })
}

module.exports = {
  findComplexRoute: findComplexRoute,
  findRouteBetweenTwoID: findRouteBetweenTwoID
};