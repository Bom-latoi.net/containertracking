let {Schema, model} = require('mongoose');

let vertexSchema = new Schema({
  _id: Number,
  coordinate: Array,
  area: Array,
  type: Number
  }, {collection: 'Vertexes'}
)

module.exports = model('Vertex', vertexSchema);