let {Schema, model} = require('mongoose');

let adjacentList = new Schema({
  _id: Number,
  adjacentVertex: [{
    _id: Number,
    weight: Number,
  }]
  }, 
  {collection: 'adjacentList'}
)

module.exports = model('adjacentList', adjacentList);