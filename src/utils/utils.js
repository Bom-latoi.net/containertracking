const euclidDistance = (coordinate1, coordinate2) => {
  return Math.sqrt((coordinate1[0] - coordinate2[0])**2 + (coordinate1[1] - coordinate2[1])**2);
}

const getArea = (coordinate) => {
  return [Math.ceil((90-coordinate[0])/5), Math.ceil((180-coordinate[1])/5)];
}

module.exports = {
  euclidDistance: euclidDistance, 
  getArea: getArea
};