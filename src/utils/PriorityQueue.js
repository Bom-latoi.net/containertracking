class PriorityQueue{
  constructor(comparator = (a,b) => a < b){
    this.heap = [];
    this.numOfElements = 0;
    this.comparator = comparator;
  }

  enqueue(data, prior){
    /*******************************************
     *  node: {
     *    currNode: nodeId of current node 
     *    path: from start node to current node
     *    prior: distance of path
     *  }  
    ********************************************/
    data.prior = prior
    this.heap.push(data);
    this.numOfElements += 1;
    this.heapUp(this.numOfElements - 1);
  }

  dequeue(){
    if (this.numOfElements === 0) throw 'Queue is empty';

    
    this.swap(0, this.numOfElements - 1);
    this.numOfElements -= 1;
    this.heapDown(0);
    return this.heap.pop();
  }

  heapDown(i){
    let left, right, largest;
    while(i < this.numOfElements - 1){
      left = 2*i + 1;
      right = 2*i + 2;
      largest = i;

      if((left < this.numOfElements) && this.comparator(this.heap[left].prior,this.heap[i].prior))
        largest = left;

      if((right < this.numOfElements) && this.comparator(this.heap[right].prior,this.heap[largest].prior))
        largest = right;

      if(largest !== i){
        this.swap(i, largest);
        i = largest;
      }
      else{
        break;
      }
    }
  }

  heapUp(i){
    let parent;
    while(i > 0){
      parent = Math.floor((i-1)/2);
      if(this.comparator(this.heap[i].prior, this.heap[parent].prior)){
        this.swap(i, parent);
        i = parent;
      }
      else{
        break;
      }
    }
  }

  size(){
    return this.numOfElements;
  }

  isEmpty(){
    return this.numOfElements === 0;
  }

  swap(i, j){
    [this.heap[i], this.heap[j]] = [this.heap[j], this.heap[i]];
  }

  display(){
    console.log(this.heap);
  }
}

module.exports = PriorityQueue;