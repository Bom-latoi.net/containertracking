const PriorityQueue = require('./PriorityQueue');

class Graph {
  constructor() {
    /* 
     *  this.nodes = [{id: Integer, coordinate: [lat, long]}]  
     * 
     *  this.edges = {nodeID1: Array, nodeID2: Array}
     *  this.edges[nodes] = [{
     *    adjacentNodeid: Integer, 
     *    weight: Float}]
    */
    this.id = 0;
    this.edges = {};
    this.nodes = {};
  }

  addNode(node) {
    this.nodes[node.id] = node.coordinate;
    this.edges[node.id] = [];
  }

  addEdge(node1, node2, weight = 1) {
    this.edges[node1.id].push({ id: node2, weight: weight });
    this.edges[node2.id].push({ id: node1, weight: weight });
  }

  addDirectedEdge(srcID, desID, weight = 1) {
    this.edges[srcID].push({ id: desID, weight: weight });
  }

  display() {
    console.log(this.nodes);
    console.log("===================");
    console.log(this.edges);
    console.log("===================");
  }

  UCS(fromID, toID) {   
    let pq = new PriorityQueue();
    let explored = new Set();
    pq.enqueue({id: fromID, path: [fromID]}, 0);
    while (!pq.isEmpty()) {
      let currVertex = pq.dequeue();
      explored.add(currVertex.id);
      if (currVertex.id === toID){
        let pathCoordinate = currVertex.path.map(id => this.nodes[id]);

        return pathCoordinate;
        //return currVertex.path;
      }
      else{
        this.edges[currVertex.id]
        .filter(adjacentVertex => !explored.has(adjacentVertex.id))
        .forEach(adjacentVertex => {
          let weight = currVertex.prior + adjacentVertex.weight;
          let path = [...currVertex.path,adjacentVertex.id];
          pq.enqueue({
            id: adjacentVertex.id,
            path: path,
          }, weight)
        });
      } 
    }
  }
}


module.exports = Graph;