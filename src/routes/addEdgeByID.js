const express = require('express');
const addEdgeByIDFunction = require('../functions/addEdgeByIDFunction');
const router = express.Router();

router.post('/', (req, res) => {
  const {listID} = req.body;
  if(typeof listID === "string"){
    addEdgeByIDFunction(JSON.parse(listID));
  }
  else{
    addEdgeByIDFunction(listID);
  }
  
  res.json('add edge/multi edges done!');
})

module.exports = router;