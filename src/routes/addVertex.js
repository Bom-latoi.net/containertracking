const express = require('express');
const addVertex = require('../functions/addVertexFunction');
const router = express.Router();

router.post('/', (req, res) => {
  const {list} = req.body;
  if(typeof list === "string"){
    addVertex(JSON.parse(list));
  }
  else{
    addVertex(list);
  }
  res.json('DONE');
})

module.exports = router;