var map;
var markersList = [];
var polylinesList = [];

function initMap() {
    var mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(0, 60),
    }

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var polyOptions = {
        geodesic: false,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        editable: true,
    }
    
    var poly = new google.maps.Polyline(polyOptions);
    poly.setMap(map);

    google.maps.event.addListener(map, "click", function(event) {
        var path = poly.getPath();
        path.push(event.latLng);

        var coordinates_poly = poly.getPath().getArray();
        var newCoordinates_poly = [];
        for (var i = 0; i < coordinates_poly.length; i++) {
            lat_poly = coordinates_poly[i].lat();
            lng_poly = coordinates_poly[i].lng();

            latlng_poly = [lat_poly, lng_poly];
            newCoordinates_poly.push(latlng_poly);
        }
        var str_coordinates_poly = JSON.stringify(newCoordinates_poly);
        
        var json_poly = str_coordinates_poly;

        document.getElementById('cor-list').value = json_poly;
    });
    
    function drawArea() {
        for (let i = -180; i < 180; i += 5) {
            let longitude = [
                { lat: 85, lng: i },
                { lat: -85, lng: i }
            ]

            let flightPath = new google.maps.Polyline({
                path: longitude,
                strokeColor: '#351fdb',
                strokeOpacity: 0.5,
                strokeWeight: 2
            });

            flightPath.setMap(map);
        }

        for (let i = -80; i < 90; i += 5) {
            let latitude = [
                { lat: i, lng: -180 },
                { lat: i, lng: 0 },
                { lat: i, lng: 180 }
            ]
            let flightPath = new google.maps.Polyline({
                path: latitude,
                strokeColor: '#a11cad',
                strokeOpacity: 0.5,
                strokeWeight: 2
            });

            flightPath.setMap(map);
        }
    }

    drawArea();
    drawAllRoute();
    markAllVertexRoute();
}

const URL = 'http://localhost:3000/';
const addCoordinatesBtn = document.getElementById('addCoordinatesBtn');
const addConnectionsBtn = document.getElementById('addConnectionsBtn');
const addPortBtn = document.getElementById('addPortBtn');

const coordinatesListTxt = document.getElementById('cor-list');
const nodeIDListTxt = document.getElementById('id-list');
const portNameTxt = document.getElementById('port-name');
const portCodeTxt = document.getElementById('port-code');
const portCoorIdTxt = document.getElementById('port-coor-id');

const addCoordinates = (event) => {
	let coordinatesList = coordinatesListTxt.value;
	fetch(URL + 'addVertex', {
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			list: coordinatesList
		})
	})
	.then(response => response.json())
	.then(console.log)
	.catch(console.error);
	console.log(coordinatesList);
	coordinatesListTxt.value = '';
}

const addConnections = (event) => {
	let listID = nodeIDListTxt.value;
	fetch(URL + 'addEdgeByID', {
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			listID: listID
		})
	})
	.then(response => response.json())
	.then(console.log)
	.catch(console.error);
	console.log(listID);
}

const addPort = (event) => {
	let portName = portNameTxt.value;
	let portCodeID = portCodeTxt.value;
	let portCoordinateID = portCoorIdTxt.value;
	fetch(URL + 'addPort', {
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			port: {
				name: portName,
				codeID: portCodeID,
				coordinateID: portCoordinateID
			}
		})
	})
	.then(response => response.json())
	.then(console.log)
	.catch(console.error);
}

addCoordinatesBtn.addEventListener('click', addCoordinates);
addConnectionsBtn.addEventListener('click', addConnections);
addPortBtn.addEventListener('click', addPort);

const markAllVertexRoute = () => {
    fetch(URL + 'getAllVertexes')
        .then(response => response.json())
        .then(data => {
            data.forEach(vertex => {
                mark(vertex)
            })
        })
}

 const mark = (vertex) => {
    let latLng = { lat: vertex.coordinate[0], lng: vertex.coordinate[1] };
    let image = {
        url: 'point.png',
        anchor: new google.maps.Point(5, 5),
        scaledSize: new google.maps.Size(10, 10)
    };

    let marker = new google.maps.Marker({
        position: latLng,
        title: `ID: ${vertex._id}`,
        icon: image,
        map: map
    });

    markersList.push(marker);
}

const removeAllMarker = () => {
	for (let i = 0; i < markersList.length; i++) {
	  markersList[i].setMap(null);
	}
}

const drawPolyline = path => {
    var flightPlanCoordinates = [];
    path.forEach(node => {
    	flightPlanCoordinates.push({
    		lat: node[0],
    		lng: node[1]
    	})
    })

    var flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: '#00FF00',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    flightPath.setMap(map);
    polylinesList.push(flightPath);
}

const removeAllPolylines = () => {
	for (let i = 0; i < polylinesList.length; i++) {
	  polylinesList[i].setMap(null);
	}
}

const drawAllRoute = () => {
	fetch(URL + 'getAllEdges')
		.then(response => response.json())
		.then(data => {
			data.edgesList.forEach(drawPolyline);
		})
}

